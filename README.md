<h1 align = "center"> :fast_forward: :recycle: Ecoleta :recycle: :rewind: </h1>

## 🖥 Preview
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117577889_1699608233526854_8753814521920290841_n.jpg?_nc_cat=107&_nc_sid=0debeb&_nc_eui2=AeFdAKVlGBjRenHXIFnXEB_5B-ytVG3P5_MH7K1Ubc_n8wP7ako8xA2YL2lhAXMFvtF4SenJE5VP541ODZrT276w&_nc_ohc=0DUzaLr2qxIAX-ojVLy&_nc_ht=scontent.fbnu2-1.fna&oh=d6860180fef7a9820d1b2fa57eb9564f&oe=5F57EC82" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117534513_1699608253526852_6033548293082378975_n.jpg?_nc_cat=102&_nc_sid=0debeb&_nc_eui2=AeHLaZF2VqMXd8HTOtGk6U0STMrgTZCM4mdMyuBNkIziZ3tD6hEIQqHg_JnxvX2wMM7LhDfvR0u76PRi0hBobpDt&_nc_ohc=0lzEfYMN0PYAX9YMg6a&_nc_ht=scontent.fbnu2-1.fna&oh=ff97ce9ebc66ff29a8fddd9b204f0e4a&oe=5F59BF6D" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117729851_1699608240193520_4365672670659010491_n.jpg?_nc_cat=102&_nc_sid=0debeb&_nc_eui2=AeGezsB7P9YTO4BB7BR5SdZzyEXqPUeLZpLIReo9R4tmkoeyIAcYVk6UH2KnMd0bXlEAYK_-Xhdj4im9dR7BkBuC&_nc_ohc=swnM1Tbm2XEAX-nvCDK&_nc_ht=scontent.fbnu2-1.fna&oh=d36a7678f3ae1dd87075dbeac5254a3b&oe=5F5B8795" width = "500">
</p>
<p align = "center">
  <img src = "https://scontent.fbnu2-1.fna.fbcdn.net/v/t1.0-9/117350678_1699608293526848_2480250647606646845_n.jpg?_nc_cat=106&_nc_sid=0debeb&_nc_eui2=AeFDwLdZONzgC--YBBChZtcpSpJ3QuYx9ltKkndC5jH2W_0wcFTg7YyNqgwu0aQvjCJGOnkv7_yw5f4gHbBRmjVQ&_nc_ohc=Qh92yXUZyeYAX_mRyf6&_nc_ht=scontent.fbnu2-1.fna&oh=3b646a86b8fdc313ef95adbdcaaad457&oe=5F5AC56B" width = "500">
</p>

---

## 📖 About
In this first edition, the Ecoleta project was developed, which serves to manage the garbage collection process in cities. With it, you can help people find collection points more efficiently.

The project was taught by Diego Schell Fernandes, Mayk Brito and the Rocketseat staff.

---

## 🛠 Technologies used
- CSS
- HTML
- Javascript
- Express
- Nunjucks
- Node.js
- SQLite

---

## 🚀 How to execute the project
#### Clone the repository
git clone https://github.com/EPieritz/ecoleta-nlw.git

#### Enter directory
`cd ecoleta-nlw`

#### Download dependencies
`npm install`

#### Run the server
`npm start`

#### That done, open your browser and go to `https://localhost:3000/`

---
Developed with 💙 by Emilyn C. Pieritz

